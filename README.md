**GUIDESMITHS INTERVIEW CODE CHALLENGE**

REST API

This is a Node Express REST API.

* Node: v10.16.3
* Express: 4.16.1


Clone this repo:
* git clone https://gitlab.com/sergioBertomeuAratech/guidesmiths_node_phone_catalogue.git
* cd guidesmiths_node_phone_catalogue


Install package dependencies

* npm install 

Run

* npm start 

Check server
* http://localhost:3000/phones
* http://localhost:3000/phones/0
