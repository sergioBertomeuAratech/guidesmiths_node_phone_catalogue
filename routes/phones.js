var express = require('express');
var router = express.Router();
var phones = require('../public/json/phones');

/* GET phone listing. */
router.get('/', function (req, res) {
    res.json(phones);
});

router.get('/:id', function (req, res) {
    const itemId = parseInt(req.params.id);
    const item = phones.find((_item) => {
        return _item.id === itemId
    });

    if (item) {
        res.json(item);
    } else {
        res.json({ message: `item ${itemId} doesn't exist` })
    }
});

module.exports = router;